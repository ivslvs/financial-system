from django.contrib import admin
from django.urls import include
from django.urls import path


urlpatterns = [
    path('admin/', admin.site.urls),
    path('rest-auth/', include('rest_auth.urls')),

    path('api/', include([
        path('users/', include('users.urls')),
        path('accounts/', include('accounts.urls')),

    ]),
         ),
]
