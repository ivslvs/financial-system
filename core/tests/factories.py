from django.contrib.auth import get_user_model
import factory

from accounts.models import BankAccount, TransactionHistory


User = get_user_model()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User


class TransactionHistoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = TransactionHistory


class BankAccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = BankAccount