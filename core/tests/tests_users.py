from rest_framework import status
from rest_framework.reverse import reverse
from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase, APIClient

from tests.factories import UserFactory


User = get_user_model()


class CustomerRegistrationTest(APITestCase):
    """ Tests for Customer registration"""

    def setUp(self):

        self.post_data = {
            'email': 'test_user@example.com',
            'password': 'test12345',
            'first_name': 'First',
            'last_name': 'Last',
        }

        UserFactory(email='awaiting_user@example.com',
                    password='test12345',
                    user_role='customer')
        UserFactory(email='declined_user@example.com',
                    password='test12345',
                    status='declined',
                    user_role='customer')

    def test_customer_registration_with_invalid_data(self):
        """Test registration with invalid data"""

        response = self.client.post(reverse('users:customer-register'), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_customer_registration_success(self):
        """Test registration success"""

        response = self.client.post(reverse('users:customer-register'), self.post_data)
        self.assertIn('token', response.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_customer_registration_create_user(self):
        """Test registration with valid data"""

        response = self.client.post(reverse('users:customer-register'), self.post_data)
        user = User.objects.filter(id=response.data['user']['pk']).first()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.post_data['email'], user.email)
        self.assertEqual(self.post_data['first_name'], user.first_name)
        self.assertEqual(self.post_data['last_name'], user.last_name)
        self.assertEqual(user.user_role, 'customer')
        self.assertEqual(User.AWAITING, user.status)
        self.assertIn('token', response.data)


class CustomerLogoutTest(APITestCase):
    """ Tests for Customer logout."""

    def test_customer_logout(self):
        """Test logout."""

        response = self.client.post(reverse('rest_logout'), {})
        self.client.credentials()
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UserResetPasswordTest(APITestCase):
    """ Tests for Customer registration."""

    def setUp(self):
        super().setUp()

        self.post_data = {
            'email': 'test_user@example.com',
            'password': 'new_password',
            'validation_code': 'TEST12',
        }

    def test_reset_password_with_invalid_data(self):
        """Test reset password with invalid data."""

        response = self.client.post(reverse('rest_password_reset'), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_reset_password_success(self):
        """Test reset password success."""

        post_data = self.post_data.copy()
        post_data.pop('password')
        post_data.pop('validation_code')
        response = self.client.post(reverse('rest_password_reset'), post_data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_reset_password_confirm_success(self):
        """Test reset password confirm success."""

        response = self.client.post(reverse('rest_password_reset'), self.post_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class EmployeeRegistrationTest(APITestCase):
    """ Tests for Employee registration."""

    def setUp(self):
        super().setUp()

        self.post_data = {
            'email': 'employee_test_user@example.com',
            'password': 'test12345',
            'first_name': 'First',
            'last_name': 'Last',
        }

    def test_employee_registration_with_invalid_data(self):
        """Test registration with invalid data."""

        response = self.client.post(reverse('users:employee-register'), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_employee_registration_success(self):
        """Test registration success."""

        response = self.client.post(reverse('users:employee-register'), self.post_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_employee_registration_create_user(self):
        """Test registration with valid data."""

        response = self.client.post(reverse('users:employee-register'), self.post_data)

        user = User.objects.first()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(self.post_data['email'], user.email)
        self.assertEqual(self.post_data['first_name'], user.first_name)
        self.assertEqual(self.post_data['last_name'], user.last_name)
        self.assertEqual(user.user_role, 'employee')
        self.assertEqual(User.AWAITING, user.status)


class EmployeeLoginTest(APITestCase):
    """ Tests for Employee login."""

    def setUp(self):

        self.user_credentials = {
            'email': 'employee_test_user@example.com',
            'password': 'test12345',
        }

    def test_employee_login_with_invalid_data(self):
        """Test login with invalid data."""

        response = self.client.post(reverse('users:employee-login'), {})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_employee_login_user_not_exist(self):

        """Test login with employee not exist"""

        credentials = {
            'email': 'not_exist_user@example.com',
            'password': 'test12345',
        }
        response = self.client.post(reverse('users:employee-login'), credentials)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn('status', response.data)
        self.assertIn('not exist', response.data['status'])


class EmployeeLogoutTest(APITestCase):
    """Tests for Employee logout"""

    def test_employee_logout(self):
        """Test logout."""

        response = self.client.post(reverse('rest_logout'), {})
        self.client.credentials()
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class CustomerTestCase(APITestCase):
    """Test Customer ViewSet"""

    def setUp(self):

        self.post_data = {
            'email': 'test_user@example.com',
            'password': 'test12345',
            'first_name': 'First',
            'last_name': 'Last',
        }

        response = self.client.post(reverse('users:customer-register'), self.post_data)
        self.token = self.client.credentials(HTTP_AUTHORIZATION=f'JWT {response.data["token"]}')

    def test_get_customer_detail(self):
        url = reverse('users:customer-detail', kwargs={'pk': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], 1)
