from rest_framework import status
from rest_framework.reverse import reverse
from django.contrib.auth import get_user_model
from rest_framework.test import APITestCase, APIClient
import factory

from tests.factories import UserFactory, BankAccountFactory, TransactionHistoryFactory
from accounts.models import BankAccount, TransactionHistory


User = get_user_model()


class BaseAPITestCase(APITestCase):

    def setUp(self):

        self.post_data = {
            'email': 'employee_test_user@example.com',
            'password': 'test12345',
            'first_name': 'First',
            'last_name': 'Last',
        }

        UserFactory(email='customer1@example.com',
                    password='test12345',
                    status='approved',
                    user_role='customer')

        UserFactory(email='customer2@example.com',
                    password='test12345',
                    status='approved',
                    user_role='customer')

        BankAccountFactory(
            account_type="deposit",
            interest_rate=5,
            customer=User.objects.filter(id=1).first(),
            period="1",
            expire_date="2021-07-18T05:21:32.120114Z",
            balance=150
        )

        BankAccountFactory(
            account_type="deposit",
            interest_rate=5,
            customer=User.objects.filter(id=1).first(),
            period="1",
            expire_date="2021-07-18T05:21:32.120114Z",
        )

        TransactionHistoryFactory(
            account_owner=BankAccount.objects.filter(id=1).first(),
            account_to=BankAccount.objects.filter(id=1).first(),
            transaction_type='withdrawal',
            amount='20'
        )
        TransactionHistoryFactory(
            account_owner=BankAccount.objects.filter(id=1).first(),
            account_to=BankAccount.objects.filter(id=1).first(),
            transaction_type='Refill',
            amount='15'
        )

        response = self.client.post(reverse('users:employee-register'), self.post_data)
        self.token = self.client.credentials(HTTP_AUTHORIZATION=f'JWT {response.data["token"]}')


class EmployeeBankAccount(BaseAPITestCase):
    """Tests for customer's bank account creation"""

    def test_customer_bank_account_create(self):
        """Test customer bank account creation"""

        url = reverse('accounts:employee-bank-account-list', self.token)
        response = self.client.post(url, {"account_type": "deposit",
                                          "interest_rate": 5,
                                          "customer": 1,
                                          "period": "6"
                                          })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_get_customer_balance_success(self):
        """Test get customer balance"""

        url = reverse('accounts:employee-bank-account-detail', self.token, kwargs={"pk": 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['balance'], '150.00')

    def test_get_customer_balance(self):
        """Test get customer invalid balance"""

        url = reverse('accounts:employee-bank-account-detail', self.token, kwargs={"pk": 2})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['balance'], '0.00')


class EmployeeTransactionHistoryTest(BaseAPITestCase):
    """Tests for employee customer's transaction history read only"""

    def test_employee_customer_transaction_history_detail(self):
        """Test customer transaction history detail"""

        url = reverse('accounts:employee-transaction-history-detail', self.token, kwargs={"pk": 1})
        response = self.client.get(url)
        transaction = TransactionHistory.objects.filter(id=response.data['id']).first()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['id'], transaction.id)
        self.assertEqual(response.data['transaction_type'], transaction.transaction_type)
        self.assertEqual(response.data['amount'], '20.00')


class EmployeeTransferMoneyTest(BaseAPITestCase):
    """ Tests for employee customer's transfer money"""

    def test_employee_transfer_customer_money_success(self):
        """Test customer transfer money success"""

        url = reverse('accounts:employee-transfer-money-detail', self.token, kwargs={"pk": 1})
        response = self.client.patch(url, {'account_to': 2, 'account_owner': 1, 'balance': 3})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'id': 1, 'balance': '147.00'})

    def test_employee_transfer_customer_insufficient_funds(self):
        """Test customer transfer insufficient funds"""

        url = reverse('accounts:employee-transfer-money-detail', self.token, kwargs={"pk": 1})
        response = self.client.patch(url, {'account_to': 1, 'account_owner': 2, 'balance': 3})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, {'message': ['You have insufficient funds in account']})
