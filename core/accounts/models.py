from django.contrib.auth import get_user_model
from django.db import models
from djmoney.contrib.django_rest_framework import MoneyField
import datetime

from .utils import generate_random_account_number


User = get_user_model()


class BankAccount(models.Model):
    """Bank account"""

    CURRENT = 'current'
    DEPOSIT = 'deposit'
    SAVINGS = 'savings'
    SALARY = 'salary'

    ACCOUNT_TYPE_CHOICES = (
        (CURRENT, 'Current'),
        (DEPOSIT, 'Deposit'),
        (SAVINGS, 'Savings'),
        (SALARY, 'Salary')
    )

    APPROVED = 'approved'
    DECLINED = 'declined'
    AWAITING = 'awaiting'
    DISABLED = 'disabled'

    STATUS_CHOICES = (
        (APPROVED, 'Approved'),
        (DECLINED, 'Declined'),
        (AWAITING, 'Awaiting'),
        (DISABLED, 'Disabled')
    )

    ONE_MONTH = '1'
    THREE_MONTHS = '3'
    SIX_MONTHS = '6'
    TWELVE_MONTHS = '12'

    PERIOD_CHOICES = (
        (ONE_MONTH, '1'),
        (THREE_MONTHS, '3'),
        (SIX_MONTHS, '6'),
        (TWELVE_MONTHS, '12'),
    )

    customer = models.ForeignKey(User, on_delete=models.CASCADE, limit_choices_to={'user_role': 'customer'})
    account_number = models.PositiveIntegerField(unique=True, default=generate_random_account_number)
    account_type = models.CharField(choices=ACCOUNT_TYPE_CHOICES, max_length=30)
    balance = models.DecimalField(max_digits=14, decimal_places=2, default=0)
    interest_rate = models.PositiveIntegerField()
    status = models.CharField(choices=STATUS_CHOICES, max_length=30, default=AWAITING)
    period = models.CharField("Period in months", choices=PERIOD_CHOICES, max_length=30)
    creation_date = models.DateTimeField(auto_now=True)
    expire_date = models.DateTimeField()

    def __str__(self):
        return f'{self.customer} | {self.account_type}'


class TransactionHistory(models.Model):
    """Transaction history"""

    WITHDRAWAL = 'withdrawal'
    REFILL = 'refill'

    TRANSACTION_TYPE = (
        (WITHDRAWAL, 'Withdrawal'),
        (REFILL, 'Refill')
    )

    account_owner = models.ForeignKey(BankAccount, on_delete=models.CASCADE, related_name='account_owner')
    account_to = models.ForeignKey(BankAccount, on_delete=models.CASCADE, blank=True, null=True)
    transaction_type = models.CharField(choices=TRANSACTION_TYPE, max_length=30)
    amount = models.DecimalField(max_digits=14, decimal_places=2)
    transaction_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.account_owner} - {self.account_to} | {self.transaction_date} | {self.transaction_type} | {self.amount}'
