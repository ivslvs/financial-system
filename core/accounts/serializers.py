from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from django.contrib.auth import get_user_model
from dateutil.relativedelta import relativedelta
from datetime import datetime
from django.db import transaction

from .models import BankAccount, TransactionHistory


User = get_user_model()


class BankAccountSerializer(serializers.ModelSerializer):
    expire_date = serializers.DateTimeField(read_only=True)

    class Meta:
        model = BankAccount
        fields = '__all__'

    def create(self, validated_data):
        months = validated_data['period']
        validated_data['expire_date'] = datetime.today() + relativedelta(months=int(months))
        return BankAccount.objects.create(**validated_data)


class TransactionHistorySerializer(ModelSerializer):
    account_owner = serializers.SerializerMethodField()
    account_to = serializers.SerializerMethodField()

    class Meta:
        model = TransactionHistory
        fields = '__all__'

    def get_account_owner(self, instance):
        return f'{instance.account_owner.customer.first_name} {instance.account_owner.customer.last_name}'

    def get_account_to(self, instance):
        return f'{instance.account_to.customer.first_name} {instance.account_to.customer.last_name}'


class TransferMoneySerializer(serializers.ModelSerializer):
    account_owner = serializers.IntegerField(required=True, write_only=True)
    account_to = serializers.IntegerField(required=True, write_only=True)

    class Meta:
        model = BankAccount
        fields = ['id', 'account_owner', 'account_to', 'balance']

    def validate(self, data):
        account_pk = (self.context.get("request").parser_context['kwargs']['pk'])
        user = self.context['request'].user

        amount = data.get('balance')
        account_owner = BankAccount.objects.filter(id=data.get('account_owner')).first()

        if user.is_customer:
            if int(account_pk) != account_owner.id:
                raise serializers.ValidationError({
                    'message': 'Wrong account id'
                })
            elif not BankAccount.objects.filter(customer=user.id, id=account_owner.id):
                raise serializers.ValidationError({
                    'message': 'You can transfer money only from your account'
                })

        if not account_owner.balance >= amount:
            raise serializers.ValidationError({
                'message': 'You have insufficient funds in account'
            })

        return data

    @transaction.atomic
    def update(self, instance, validated_data):
        account_owner = BankAccount.objects.filter(id=validated_data['account_owner']).first()
        account_to = BankAccount.objects.filter(id=validated_data['account_to']).first()
        amount = self.validated_data['balance']

        account_owner.balance -= amount
        account_owner.save()
        TransactionHistory.objects.create(
            account_owner=account_owner,
            account_to=account_to,
            transaction_type='withdrawal',
            amount=amount * (-1)
        )

        account_to.balance += amount
        account_to.save()
        TransactionHistory.objects.create(
            account_owner=account_to,
            account_to=account_owner,
            transaction_type='refill',
            amount=amount
        )

        return account_owner
