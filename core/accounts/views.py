from rest_framework.viewsets import ModelViewSet
from django.contrib.auth import get_user_model
from rest_framework.exceptions import ValidationError
from rest_framework import mixins
from rest_framework import viewsets
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from .models import BankAccount, TransactionHistory
from users.permissions import IsCustomerUser, IsEmployeeUser
from .serializers import (
    BankAccountSerializer, TransactionHistorySerializer, TransferMoneySerializer
)

User = get_user_model()


class CustomerBankAccountReadOnly(ReadOnlyModelViewSet):
    """Customer's bank account ReadOnly"""

    queryset = BankAccount.objects.all()
    serializer_class = BankAccountSerializer
    permission_classes = [IsCustomerUser]

    def get_queryset(self):
        return self.queryset.filter(customer=self.request.user.id)

    def retrieve(self, request, pk=None):  # retrieve balance
        account_owner = get_object_or_404(self.queryset, pk=pk)
        serializer = BankAccountSerializer(account_owner)

        if account_owner.account_type == 'deposit' and account_owner.balance < 100:
            raise ValidationError(
                {'message': 'To activate your deposit account, please transfer to the account 100 c.u.',
                 'account_owner': serializer.data
                 })

        return Response(serializer.data)


class EmployeeBankAccountViewSet(ModelViewSet):
    """Customer's bank account ViewSet"""

    queryset = BankAccount.objects.all()
    serializer_class = BankAccountSerializer
    permission_classes = [IsEmployeeUser]


class CustomerTransactionHistoryReadOnly(ReadOnlyModelViewSet):
    """Customer's account history ReadOnly"""

    queryset = TransactionHistory.objects.all()
    serializer_class = TransactionHistorySerializer
    permission_classes = [IsCustomerUser]

    def list(self, request, *args, **kwargs):
        accounts_owner = BankAccount.objects.filter(customer=request.user.id)
        accounts_history = []
        for account in accounts_owner:
            accounts_history.extend(TransactionHistory.objects.filter(account_owner=account))

        serializer = self.get_serializer(accounts_history, many=True)
        return Response(serializer.data)


class EmployeeTransactionHistoryReadOnly(ReadOnlyModelViewSet):
    """Customer's account history ReadOnly"""

    queryset = TransactionHistory.objects.all()
    serializer_class = TransactionHistorySerializer
    permission_classes = [IsEmployeeUser]

    def list(self, request, *args, **kwargs):
        accounts_owner = BankAccount.objects.filter(customer=request.data.get('id'))
        accounts_history = []
        for account in accounts_owner:
            accounts_history.extend(TransactionHistory.objects.filter(account_owner=account))

        serializer = self.get_serializer(accounts_history, many=True)
        return Response(serializer.data)


class CustomerTransferMoneyUpdate(mixins.UpdateModelMixin,
                                viewsets.GenericViewSet):
    """Customer is able to transfer money"""

    queryset = BankAccount.objects.all()
    serializer_class = TransferMoneySerializer
    permission_classes = [IsCustomerUser]


class EmployeeTransferMoneyUpdate(mixins.UpdateModelMixin,
                                viewsets.GenericViewSet):
    """Bank employee is able to transfer customer's money"""

    queryset = BankAccount.objects.all()
    serializer_class = TransferMoneySerializer
    permission_classes = [IsEmployeeUser]
