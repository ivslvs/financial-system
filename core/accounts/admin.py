from django.contrib import admin
from .models import BankAccount, TransactionHistory


admin.site.register([BankAccount, TransactionHistory])
