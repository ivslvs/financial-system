from rest_framework.routers import DefaultRouter
from django.urls import path

from .views import (
    CustomerBankAccountReadOnly, CustomerTransactionHistoryReadOnly, CustomerTransferMoneyUpdate,
    EmployeeTransferMoneyUpdate, EmployeeTransactionHistoryReadOnly, EmployeeBankAccountViewSet
)


app_name = 'accounts'

router = DefaultRouter()
router.register('customers/bank_accounts', CustomerBankAccountReadOnly, basename='customer-bank-account')
router.register('employees/bank_accounts', EmployeeBankAccountViewSet, basename='employee-bank-account')

router.register('customers/accounts_history', CustomerTransactionHistoryReadOnly,
                basename='customer-transaction-history')
router.register('employees/accounts_history', EmployeeTransactionHistoryReadOnly,
                basename='employee-transaction-history')

router.register('customers/transfers', CustomerTransferMoneyUpdate, basename='customer-transfer-money'),
router.register('employees/transfers', EmployeeTransferMoneyUpdate, basename='employee-transfer-money'),


urlpatterns = [

]
urlpatterns += router.urls
