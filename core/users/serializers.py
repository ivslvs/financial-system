from rest_framework import serializers
from rest_framework.serializers import ModelSerializer
from django.contrib.auth import get_user_model
from rest_auth.registration.serializers import RegisterSerializer
from rest_auth.serializers import LoginSerializer


User = get_user_model()


class EmployeeRegistrationSerializer(RegisterSerializer):
    username = None
    password1 = None
    password2 = None
    email = serializers.EmailField(required=True, max_length=254)
    password = serializers.CharField(
        write_only=True, label='Enter your password',
        style={'input_type': 'password'},
    )
    first_name = serializers.CharField(required=True, max_length=30)
    last_name = serializers.CharField(required=True, max_length=150)
    user_role = serializers.HiddenField(default=User.EMPLOYEE)

    def validate_email(self, email):
        return email

    def validate(self, data):
        email = data.get('email')
        user = User.objects.filter(email__iexact=email).first()
        if user and user.status in [User.DISABLED, User.AWAITING, User.DECLINED]:
            raise serializers.ValidationError({
                'status': user.status,
                'message': ["We can't complete your request now. Please try again later."]
            })
        return data

    def save(self, request):
        user = User.objects.create_user(
            email=self.validated_data['email'].lower(),
            first_name=self.validated_data['first_name'],
            last_name=self.validated_data['last_name'],
            password=self.validated_data['password'],
            user_role=self.validated_data['user_role'],
        )
        return user


class CustomerRegistrationSerializer(RegisterSerializer):
    username = None
    password1 = None
    password2 = None
    email = serializers.EmailField(required=True, max_length=254)
    password = serializers.CharField(
        write_only=True, label='Enter your password',
        style={'input_type': 'password'},
    )
    first_name = serializers.CharField(required=True, max_length=30)
    last_name = serializers.CharField(required=True, max_length=150)
    user_role = serializers.HiddenField(default=User.CUSTOMER)

    def validate_email(self, email):
        return email

    def validate(self, data):
        email = data.get('email')
        user = User.objects.filter(email__iexact=email).first()
        if user and user.status in [User.DISABLED, User.AWAITING, User.DECLINED]:
            raise serializers.ValidationError({
                'status': user.status,
                'message': ["We can't complete your request now. Please try again later."]
            })
        return data

    def save(self, request):
        user = User.objects.create_user(
            email=self.validated_data['email'].lower(),
            first_name=self.validated_data['first_name'],
            last_name=self.validated_data['last_name'],
            password=self.validated_data['password'],
            user_role=self.validated_data['user_role'],
        )
        return user


class EmployeeLoginSerializer(LoginSerializer):
    username = None
    email = serializers.EmailField()

    def validate(self, attrs):
        validated_attrs = super(EmployeeLoginSerializer, self).validate(attrs)
        print(validated_attrs)
        if self.user and self.user.status in [User.DISABLED, User.AWAITING, User.DECLINED]:
            raise serializers.ValidationError({
                'status': self.user.status,
                'message': ["We can't complete your request now. Please try again later."]
            })
        elif not self.user.is_employee:
            raise serializers.ValidationError({
                'message': [f'You must be a bank employee to enter this app, your user role: {self.user.user_role}.'],
            })
        return validated_attrs

    def _validate_email(self, email, password):
        email = email.lower()
        if not User.objects.filter(email__iexact=email).exists():
            raise serializers.ValidationError({
                'status': 'not exist'
            })
        self.user = super(EmployeeLoginSerializer, self)._validate_email(email, password)
        return self.user


class CustomerLoginSerializer(LoginSerializer):
    username = None
    email = serializers.EmailField()

    def validate(self, attrs):
        validated_attrs = super(CustomerLoginSerializer, self).validate(attrs)
        print(validated_attrs)
        if self.user and self.user.status in [User.DISABLED, User.AWAITING, User.DECLINED]:
            raise serializers.ValidationError({
                'status': self.user.status,
                'message': ["We can't complete your request now. Please try again later."]
            })
        elif not self.user.is_customer:
            raise serializers.ValidationError({
                'message': [f'You must be a customer to enter this app, your user role: {self.user.user_role}.'],
            })
        return validated_attrs

    def _validate_email(self, email, password):
        email = email.lower()
        if not User.objects.filter(email__iexact=email).exists():
            raise serializers.ValidationError({
                'status': 'not exist'
            })
        self.user = super(CustomerLoginSerializer, self)._validate_email(email, password)
        return self.user


class EmployeeSerializer(ModelSerializer):
    user_role = serializers.HiddenField(default=User.is_employee)

    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user


class CustomerSerializer(ModelSerializer):
    user_role = serializers.HiddenField(default=User.is_customer)

    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        user = super().create(validated_data)
        user.set_password(validated_data['password'])
        user.save()
        return user
