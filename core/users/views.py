from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import AllowAny
from django.contrib.auth import get_user_model
from rest_auth.registration.views import RegisterView
from rest_auth.views import LoginView

from .permissions import IsCustomerUser, IsEmployeeUser
from .serializers import (
     CustomerSerializer, EmployeeSerializer, EmployeeRegistrationSerializer, CustomerRegistrationSerializer,
     EmployeeLoginSerializer, CustomerLoginSerializer
)


User = get_user_model()


class EmployeeRegistration(RegisterView):
    """Bank employee registration"""

    serializer_class = EmployeeRegistrationSerializer
    permission_classes = [AllowAny]


class CustomerRegistration(RegisterView):
    """Customer registartion"""

    serializer_class = CustomerRegistrationSerializer
    permission_classes = [AllowAny]


class EmployeeLoginView(LoginView):
    """Bank employee login"""

    serializer_class = EmployeeLoginSerializer


class CustomerLoginView(LoginView):
    """Customer login"""

    serializer_class = CustomerLoginSerializer


class EmployeeViewSet(ModelViewSet):
    """Bank employee ViewSet"""

    queryset = User.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = [IsEmployeeUser]

    def get_queryset(self):
        return self.queryset.filter(email=self.request.user.email)


class CustomerViewSet(ModelViewSet):
    """Customer ViewSet"""

    queryset = User.objects.all()
    serializer_class = CustomerSerializer
    permission_classes = [IsCustomerUser]

    def get_queryset(self):
        return self.queryset.filter(email=self.request.user.email)
