from django.contrib import admin
from .models import User
# from allauth.account.models import EmailAddress
from django.contrib.sites.models import Site
from django.contrib.auth.admin import Group


admin.site.register(User)

# admin.site.unregister(EmailAddress)
admin.site.unregister(Site)
admin.site.unregister(Group)
