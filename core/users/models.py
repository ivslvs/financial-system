from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import BaseUserManager
from django.db import models
from django.core.validators import RegexValidator


alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')


class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class User(AbstractUser):

    APPROVED = 'approved'
    DECLINED = 'declined'
    AWAITING = 'awaiting'
    DISABLED = 'disabled'

    STATUS_CHOICES = (
        (APPROVED, 'Approved'),
        (DECLINED, 'Declined'),
        (AWAITING, 'Awaiting'),
        (DISABLED, 'Disabled')
    )

    EMPLOYEE = 'employee'
    CUSTOMER = 'customer'

    ROLE_CHOICES = (
        (EMPLOYEE, 'Employee'),
        (CUSTOMER, 'Customer'),
    )

    username = None
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=150)
    email = models.EmailField(unique=True, max_length=254, db_index=True)
    passport_number = models.CharField(max_length=10, unique=True, blank=True, null=True, validators=[alphanumeric])
    status = models.CharField(max_length=8, choices=STATUS_CHOICES, default=AWAITING)
    user_role = models.CharField(max_length=15, choices=ROLE_CHOICES)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager()

    def __str__(self):
        return self.email

    @property
    def is_employee(self):
        return self.user_role == self.EMPLOYEE

    @property
    def is_customer(self):
        return self.user_role == self.CUSTOMER
