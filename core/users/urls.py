from django.urls import path, include
from rest_framework.routers import DefaultRouter

from .views import (
    EmployeeViewSet, CustomerViewSet, EmployeeRegistration, CustomerRegistration, EmployeeLoginView,
    CustomerLoginView
)


app_name = 'users'

router = DefaultRouter()
router.register('employees', EmployeeViewSet, basename='employee')
router.register('customers', CustomerViewSet, basename='customer')


urlpatterns = [
    path('login/employee/', EmployeeLoginView.as_view(), name='employee-login'),
    path('login/customer/', CustomerLoginView.as_view(), name='customer-login'),

    path('registration/employee/', EmployeeRegistration.as_view(), name='employee-register'),
    path('registration/customer/', CustomerRegistration.as_view(), name='customer-register')

]
urlpatterns += router.urls
